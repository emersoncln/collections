<?php
session_start();

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Method: POST');
// header('Content-type: application/json');

require '../../../vendor/autoload.php';
session_destroy();
// include_once("./src/templates/header.php")
?>
<script type="application/javascript">
    window.location.href="/src/login.php"
</script>