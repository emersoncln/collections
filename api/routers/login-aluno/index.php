<?php
session_start();

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Method: POST');
header('Content-type: application/json');

require '../../../vendor/autoload.php';

use src\Controllers\AlunoController;
use src\Controllers\QuestionarioController;

$method = strtoupper($_SERVER['REQUEST_METHOD']);

if ($method === 'POST') {

  if (!empty($_POST['cpf'])) {

    $cpf = filter_input(INPUT_POST, 'cpf');

  } else {
    $dados = file_get_contents('php://input');
    $dados = json_decode($dados, true);

    $cpf = $dados['cpf'];
  }

  if (isset($cpf)) {

    $aluno = new AlunoController();
    $array = $aluno->verificaAluno($cpf);

    if ($array) {

      $idTurma = $array['result']['User']['turma_id'];

      $questionario = new QuestionarioController();
      $questionarios = $questionario->getQuestionariosTurmaId($idTurma);

      $array['questionarios'] = $questionarios;
      $array['relatorios'] = [];

      // força a sessão do usuário
      $_SESSION["user_name"] = $array['result']['User']["nome"];
      $_SESSION["user_id"] = $array['result']['User']["cpf"];
      $_SESSION["aluno_id"] = $array['result']['User']["id"];
      $_SESSION["turma_Id"] = $idTurma;
      $_SESSION["is_professor"] = false;

      echo json_encode($array);
    } else {
      $array['success'] = false;
      $array['error'] = "CPF informado inválido.";
      http_response_code(401);
      echo json_encode($array);
    }

  } else {
    $array['success'] = false;
    $array['error'] = 'CPF não informado.';
    http_response_code(401);
    echo json_encode($array);
  }

} else {
  $array['success'] = false;
  $array['error'] = 'Método inválido. Permitido somente POST';
  http_response_code(401);
  echo json_encode($array);
}