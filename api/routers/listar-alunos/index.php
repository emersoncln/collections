<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Method: *');
header('Content-Type: application/json');

require '../../../vendor/autoload.php';

use src\Controllers\AlunoController;

$method = strtoupper($_SERVER['REQUEST_METHOD']);

if ($method === 'GET') {
    $controller = new AlunoController();
    $processores = $controller->getAlunos();
    echo json_encode($processores);
} else {

  $array['success'] = false;
  $array['error'] = 'Método inválido. Permitido apenas GET';
  echo json_encode($array);

}