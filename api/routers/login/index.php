<?php
session_start();

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");
header("Content-Text: application/json");

require '../../../vendor/autoload.php';

use src\Controllers\UsuarioController;
use src\Controllers\ProfessorController;
use src\Controllers\AlunoController;
use src\Controllers\QuestionarioController;


$method = strtoupper($_SERVER['REQUEST_METHOD']);

if ($method === 'POST') {
  if (!empty($_POST['email']) && !empty($_POST['senha'])) {

    $email = filter_input(INPUT_POST, 'email');
    $senha = filter_input(INPUT_POST, 'password');
  } else {

    $dados = file_get_contents("php://input");
    $dados = json_decode($dados, true);

    $email = $dados['email'];
    $senha = $dados['senha'];
  }

  if (!empty($email) && !empty($senha)) {

    $user = new UsuarioController();
    $array = $user->verificaUsuario($email, $senha);



    if ($array['success'] == '1' && $array['result']['User']['admin'] === 'N') {

      $userId = $array['result']['User']['id'];
      
      $questionarioController = new QuestionarioController();
      $alunoController = new AlunoController();

      $professor = (new ProfessorController())->getProfessor($userId);
      $isProfessor = $professor != null;

      if ($isProfessor) {
        $questionarios = $questionarioController->getQuestionariosProfessor($userId);
        $array['result']['questionarios'] = $questionarios;
        
        $alunos = $alunoController->getAlunosProfessor($userId);
        $array['result']['alunos'] = $alunos;
        
        $array['result']['professor'] = $professor;

        $_SESSION["user_name"] = $professor["nome"];
        $_SESSION["professor_id"] = $professor["id"];
      }
      $_SESSION["user_id"] = $userId;
      $_SESSION["is_professor"] = $isProfessor;

      echo json_encode($array);
    } elseif ($array['success'] == '1' && $array['result']['User']['admin'] === 'S') {

      $prof = new ProfessorController();
      $professores = $prof->getProfessores();
      $alunoController = new AlunoController();
      $alunos = $aluno->getAlunos();
      $questionarioController = new QuestionarioController();
      $questionarios = $questionario->getQuestionarios();

      $array['result']['professores'] = $professores;
      $array['result']['questionarios'] = $questionarios;
      $array['result']['alunos'] = $alunos;
      echo json_encode($array);
    } else if (!$array['success']) {
      $array['success'] = false;
      $array['error'] = 'Email ou senha incorreto(s).';
      http_response_code(401);
      echo json_encode($array);
    }
  }
} else {
  http_response_code(401);
  $array['success'] = false;
  $array['error'] = 'Método inválido, permitido somente POST';
  echo json_encode($array);
}


// require('../return.php');
