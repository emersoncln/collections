<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Method: *');
header('Content-Type: appplication/json');

require '../../../vendor/autoload.php';

use src\Controllers\DisciplinaController;

$method = strtoupper($_SERVER['REQUEST_METHOD']);


if ($method === 'POST') {

  if (!empty($_POST['nome'])) {

    $nome = filter_input(INPUT_POST, 'nome');
    
    $controller = new DisciplinaController();
    $id = $controller->cadastrarDisciplina($id, $nome);

  } else {
    
    $dados = file_get_contents('php://input');
    $dados = json_decode($dados, true);
    
    $nome = $dados['nome'];

    $controlle = new DisciplinaController();
    $id = $controlle->cadastrarDisciplina($id, $nome); 
  }

} else {
  $array['success'] = false;
  $array['error'] = 'Método inválido. Permitido somente POST';
  echo json_encode($array);
}
