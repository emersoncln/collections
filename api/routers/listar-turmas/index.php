<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Method: *');
header('Context-Type: application/json');

require '../../../vendor/autoload.php';

use src\Controllers\TurmaController;

$method = strtoupper($_SERVER['REQUEST_METHOD']);

if ($method === 'GET') {
    $controller = new TurmaController();
    $processores = $controller->getTurmas();
    echo json_encode($processores);
} else {

  $array['success'] = false;
  $array['error'] = 'Método inválido. Permitido apenas GET';
  echo json_encode($array);

}