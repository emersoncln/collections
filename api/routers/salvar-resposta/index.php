<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Method: *');
header('Content-Type: application/json');

require '../../../vendor/autoload.php';
session_start();
use src\Controllers\AlunoController;

$method = strtoupper($_SERVER['REQUEST_METHOD']);

if ($method === 'POST') {
    try {
        $dados = file_get_contents('php://input');
        $dados = json_decode($dados, true);

        $id = $dados['id'];
        $questItemId = $dados['questItemId'];
        $alunoId = $_SESSION["aluno_id"];

        $controller = new AlunoController();

        $idResposta = $controller->salvarResposta($alunoId, $questItemId, $id);
        http_response_code(200);
        $array['success'] = true;
        $array['error'] = null;
        $array['data'] = $idResposta;

        echo json_encode($array);
    } catch (\Exception $e) {
        http_response_code(400);
        $array['success'] = false;
        $array['error'] = $e->getMessage();
        $array['data'] = $e->getTrace();

        echo json_encode($array);
    }
} else {
    http_response_code(400);
    $array['success'] = false;
    $array['error'] = 'Método inválido. Permitido apenas POST';
    echo json_encode($array);
}
