<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Method: *');
header('Context-Type: application/json');

require '../../../vendor/autoload.php';

use src\Controllers\ProfessorController;

$method = strtoupper($_SERVER['REQUEST_METHOD']);

if ($method === 'GET') {
    $controller = new ProfessorController();
    $processores = $controller->getProfessores();
    echo json_encode($processores);
} else {

  $array['success'] = false;
  $array['error'] = 'Método inválido. Permitido apenas GET';
  echo json_encode($array);

}