<?php
session_start();

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Method: *');
header('Content-Type: appplication/json');

require '../../../vendor/autoload.php';

use src\Controllers\TurmaController;

$method = strtoupper($_SERVER['REQUEST_METHOD']);

if ($method === 'POST') {
  if (!empty($_POST['turma'])) {

    $tuma = filter_input(INPUT_POST, 'turma');
    $professor_id = $_SESSION["professor_id"];
    $controller = new TurmaController();
    try {
      $idTurma = $controller->cadastroTurma($id, $turma, $professor_id);
    } catch (\Exception $e) {
      http_response_code(400);
      $array['success'] = false;
      $array['error'] = $e->getMessage();
      $array['data'] = $e->getTrace();

      echo json_encode($array);
    }
  } else {
    try {
      $dados = file_get_contents('php://input');
      $dados = json_decode($dados, true);

      $turma = $dados['turma'];
      $professor_id = $_SESSION["professor_id"];

      $controller = new TurmaController();

      $idTurma = $controller->cadastroTurma($id, $turma, $professor_id);
    } catch (\Exception $e) {
      http_response_code(400);
      $array['success'] = false;
      $array['error'] = $e->getMessage();
      $array['data'] = $e->getTrace();

      echo json_encode($array);
    }
  }
} else {
  $array['success'] = false;
  $array['error'] = 'Método inválido. Permitido somente POST';
  echo json_encode($array);
}
