<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
<!-- JavaScript Bundle with Popper -->

<link rel="stylesheet" href="styles.css"/>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>
  <title>Cadastro de professores</title>
</head>
<body>
  <div class='container'>
  <div class="card block" style="width: 100%;">
  <h1 class='title'>Cadastre-se</h1>
  <h3 class='sub-title'>preencha os campos abaixo</h3>
  <div class="card-body w-50">
  <label for="firstName" class="form-label">Nome</label>
  <input type="text" class="form-control" id="firstName" placeholder="" value="" required="">
  <label for="lastName" class="form-label">Sobrenome</label>
  <input type="lastName" class="form-control" id="lastName" placeholder="" value="" required="">
  <label for="number" class="form-label">Telefone</label>
  <input type="tel" class="form-control phone" id="phone" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" placeholder="" value="" required="">
  <label for="email" class="form-label">E-mail</label>
  <input type="email" class="form-control" id="email" placeholder="">
  <label for="password" class="form-label">Senha</label>
  <input type="password" class="form-control" id="password" placeholder="">
  <button class="w-100 btn-primary btn-lg mt-2" type="submit">Cadastrar</button>
  <label for="firstName" class="form-label">Já tem cadastro?</label>
  <a href="" onclick="">Login</a>
  </div>
</div>
  </div>
</body>
</html>
