<?php

namespace src\Model;

use src\Core\Database;

class Disciplina
{
  private static $array = [];
  private static $con;
  private static $db;

  public function __construct()
  {
    self::$con = new Database();
    self::$db = self::$con->connect();
  }

  public function cadastrarDisciplina($id, $nome) {
    $sql = self::$db->prepare('INSERT INTO disciplina (id, nome)
    VALUES (:id, :nome)');
    $sql->bindValue(':id', $id);
    $sql->bindValue(':nome', $nome);
    $sql->execute();

    $id = self::$db->lastInsertId();
    return $id;

  }

  public function getDisciplinas()
  {
    $sql = self::$db->prepare('SELECT * FROM disciplina');
    $sql->execute();

    if ($sql->rowCount() > 0) {
      $dados = $sql->fetchAll(\PDO::FETCH_ASSOC);
      return $dados;
    }
  }

  public function getDisciplinasById($id)
  {
    $sql = self::$db->prepare("SELECT * FROM disciplina where id = $id;");
    $sql->execute();

    if ($sql->rowCount() > 0) {
      $dados = $sql->fetch(\PDO::FETCH_ASSOC);
      return $dados;
    }
  }


  public function excluirDisciplina($id) {

    $sql = self::$db->query("DELETE FROM disciplina WHERE id = ".$id);
    
  }
};