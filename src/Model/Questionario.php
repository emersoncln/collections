<?php

namespace src\Model;

use src\Core\Database;

class Questionario
{

  private static $con;
  private static $db;

  public function __construct()
  {
    self::$con = new Database();
    self::$db = self::$con->connect();
  }

  public function cadastrarQuestionario($turma, $titulo, $descricao, $disciplina_id)
  {
    echo $turma, $titulo, $descricao, $disciplina_id;
    // try {
    $sql = self::$db->prepare('INSERT INTO questionario (turma_id, titulo, descricao, disciplina_id) VALUES (:turma_id, :titulo, :descricao, :disciplina_id)');
    $sql->bindValue(':turma_id', $turma);
    $sql->bindValue(':titulo', $titulo);
    $sql->bindValue(':descricao', $descricao);
    $sql->bindValue(':disciplina_id', $disciplina_id);
    $sql->execute();

    $id = self::$db->lastInsertId();
    return $id;
  }

  public function cadastrarRespostas($idQuestionario, $respostas)
  {

    // var_dump($respostas);
    $countRespostas = count($respostas);
    echo $idQuestionario;
    for ($i = 0; $i < $countRespostas; $i++) {

      $sql = self::$db->prepare("INSERT INTO questionario_items (questao, correta, questionario_id) VALUES (:questao, :correta, :id)");
      $sql->bindValue(':questao', $respostas[$i]['questao']);
      $sql->bindValue(':correta', $respostas[$i]['correta']);
      $sql->bindValue(':id', $idQuestionario);
      $sql->execute();
    }

    return self::$db->lastInsertId();
  }

  public function getItemsQuestionarios($idQuest, $alunoId)
  {

    $sql = self::$db->prepare("SELECT 
        qi.id, qi.questao, qi.correta, qi.questionario_id, r.id as resposta_id
      from 
        questionario_items qi
      left join questionario_items_resposta r on (r.questionario_item_id = qi.id and r.aluno_id = $alunoId)
      where 
        questionario_id = $idQuest
    ");
    $sql->execute();

    if ($sql->rowCount() > 0) {
      $dados = $sql->fetchAll(\PDO::FETCH_ASSOC);
      return $dados;
    }

    return array();
  }

  public function getQuestionarios()
  {
    $sql = self::$db->prepare("SELECT * FROM questionario");
    $sql->execute();

    if ($sql->rowCount() > 0) {
      $questionarios = $sql->fetchAll(\PDO::FETCH_ASSOC);
    }
    return $questionarios;
  }

  public function getQuestionarioById($id)
  {
    $sql = self::$db->prepare("SELECT * FROM questionario where id = $id");
    $sql->execute();
    
    $questionarios = null;

    if ($sql->rowCount() > 0) {
      $questionarios = $sql->fetch(\PDO::FETCH_ASSOC);
    }
    return $questionarios;
  }



  public function getQuestionariosProfessor($id)
  {
    $sql = self::$db->prepare("SELECT turma_id, titulo, descricao, disciplina_id FROM questionario INNER JOIN turma ON turma.professor_id = :id AND questionario.turma_id = turma.id WHERE turma.professor_id = :id");
    $sql->bindValue('id', $id);
    $sql->execute();

    if ($sql->rowCount() > 0) {
      $dados = $sql->fetchAll(\PDO::FETCH_ASSOC);
      return $dados;
    }
  }

  public function getQuestionariosTurmaId($id, $alunoId = null)
  {
    $script = "SELECT 
    *, 
    D.nome as disciplina, 
    Q.id as quest_id";

    // se for aluno retorna a resposta
    if (!empty($alunoId)) {
      $script = $script . ", (
        select 
          qi.questao as respondido 
        from 
          questionario_items_resposta r 
        left join 
          questionario_items qi on (qi.id = r.questionario_item_id)
        where 
          r.aluno_id = $alunoId and qi.questionario_id = Q.id limit 1
       ) as respondido,
       (
        select 
          case when qi.correta then 'Sim' else 'Não' end as respondido_correta 
        from 
          questionario_items_resposta r 
        left join 
          questionario_items qi on (qi.id = r.questionario_item_id)
        where 
          r.aluno_id = $alunoId and qi.questionario_id = Q.id limit 1
       ) as respondido_correta";
    }

    $script = $script . " FROM 
      questionario Q 
    LEFT JOIN 
      disciplina D ON (Q.disciplina_id = D.id) 
    WHERE 
      turma_id = :id";
    $sql = self::$db->prepare($script);

    $sql->bindValue('id', $id);
    $sql->execute();

    if ($sql->rowCount() > 0) {
      $dados = $sql->fetchAll(\PDO::FETCH_ASSOC);
      return $dados;
    }

    return array();
  }
}
