<?php

namespace src\Model;

use src\Core\Database;

class Turma
{
  private static $array = [];
  private static $con;
  private static $db;

  public function __construct()
  {
    self::$con = new Database();
    self::$db = self::$con->connect();
  }

  public function cadastroTurmas($id, $turma, $professor_id) {
    $sql = self::$db->prepare('INSERT INTO turma (id, nome, professor_id)
    VALUES (:id, :nome, :professor_id)');
     $sql->bindValue(':id', $id);
    $sql->bindValue(':nome', $turma);
    $sql->bindValue(':professor_id', $professor_id);
    $sql->execute();

    $id = self::$db->lastInsertId();
    return $id;

  }

  public function getTurmas()
  {
    $sql = self::$db->prepare('SELECT * FROM turma');
    $sql->execute();

    if ($sql->rowCount() > 0) {
      $dados = $sql->fetchAll(\PDO::FETCH_ASSOC);
      return $dados;
    }
  }

  public function getTurmaById($turmaId)
  {
    $sql = self::$db->prepare("SELECT * FROM turma where id = $turmaId;");
    $sql->execute();

    if ($sql->rowCount() > 0) {
      $dados = $sql->fetch(\PDO::FETCH_ASSOC);
      return $dados;
    }
  }

  public function getTurmaByProfId($profId)
  {
    $sql = self::$db->prepare("SELECT * FROM turma where professor_id = $profId;");
    $sql->execute();

    if ($sql->rowCount() > 0) {
      $dados = $sql->fetchAll(\PDO::FETCH_ASSOC);
      return $dados;
    }
  }

  public function excluirTurma($id) {

    $sql = self::$db->query("DELETE FROM turma WHERE id = ".$id);
    
  }
};