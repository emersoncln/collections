<?php

namespace src\Controllers;

use src\Model\Disciplina;

class DisciplinaController {

  private static $array = [];
  static $disciplina;

  public function __construct(){
    self::$disciplina = new Disciplina();
  }

  public function cadastrarDisciplina($id, $nome) {

    self::$disciplina->cadastrarDisciplina($id, $nome);

  }

  public function getDisciplinas() {

    return self::$disciplina->getDisciplinas();

  }

  public function getDisciplina($id) {

    return self::$disciplina->getDisciplinasById($id);

  }

  public function excluirDisciplina($id) {
    return self::$disciplina->excluirDisciplina($id);
  }

}