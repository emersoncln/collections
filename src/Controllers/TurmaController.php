<?php

namespace src\Controllers;

use src\Model\Turma;

class TurmaController {

  private static $array = [];
  static $turma;

  public function __construct(){
    self::$turma = new Turma();
  }

  public function cadastroTurma($id, $turma, $professor_id) {

    self::$turma->cadastroTurmas($id, $turma, $professor_id);

  }

  public function getTurmas() {

    return self::$turma->getTurmas();

  }

  public function getTurma($id) {

    return self::$turma->getTurmaById($id);

  }

  public function excluirTurma($id) {
    return self::$turma->excluirTurma($id);
  }

}