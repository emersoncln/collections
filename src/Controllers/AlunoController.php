<?php

namespace src\Controllers;

use src\Model\Aluno;

class AlunoController
{

  private static $aluno;

  public function __construct()
  {
    self::$aluno = new Aluno();
  }

  public function verificaAluno($cpf)
  {
    $aluno = self::$aluno->verificaAluno($cpf);
    
    if ($aluno) {
      $array['success'] = true;
      $array['result']['User'] = $aluno;
      return $array;
    }

    return null;
  }

  public function cadastrarAluno($nome, $sobrenome, $cpf, $turma)
  {
    try {
      $val = self::$aluno->cadastrarAluno($nome, $sobrenome, $cpf, $turma);
      return $val;
    } catch (\PDOException $e) {
      return false;
    }
  }

  public function getAlunos()
  {
    $alunos = self::$aluno->getAlunos();
    return $alunos;
  }

  public function getAlunosProfessor($id)
  {
    $alunos = self::$aluno->getAlunosProfessor($id);
    // echo json_encode($alunos);
    return $alunos;
  }

  public function excluirAluno($id)
  {
    try {
      self::$aluno->excluirAluno($id);
    } catch (\PDOException $e) {
      echo "Error: " . $e->getMessage();
    }
  }

  /**
   * @param int $alunoId id do aluno
   * @param int $questItemId id do item
   * @param int $id id da resposta (usado para atualizar)
   */
  public function salvarResposta($alunoId, $questItemId, $id) {
    return self::$aluno->salvarResposta($alunoId, $questItemId, $id);
  }
}
