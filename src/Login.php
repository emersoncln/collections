<?php
  $title = "Acesso ao sistema";
  include_once("./templates/header.php");
  // pode ser alunos ou professores
  // deve renderizar os campos de login de acordo com o tipo
  // o aluno acessa apenas com cpf
  $type=filter_input(INPUT_GET, "type");
  
  $isFormAluno=false;
  if (isset($type)){
    $isFormAluno = $type == "alunos"; 
  }
?>

<div class='container'>
  <div class="card block" style="width: 100%;">
    <h1 class='title'>Collections</h1>

    <?php if (!$isLoggedIn) { ?>
      <h3 class='sub-title'>Entrar como <?php if ($isFormAluno) { echo "aluno"; } else { echo "professor";} ?></h3>
      <div class="card-body w-50">
        <form id="formSubmit" onsubmit="login" method="post">
          <?php if (!$isFormAluno) {?>
          <label for="email" class="form-label">E-mail</label>
          <input name="email" type="email" class="form-control" id="email" placeholder="" value="" required="true">
          
          <label for="password" class="form-label">Senha</label>
          <input name="password" type="password" class="form-control" id="password" placeholder="" value="" required="true">
            <?php } else {?>
              <label for="cpf" class="form-label">CPF</label>
              <input name="cpf" type="text" class="form-control" id="cpf" placeholder="informe seu CPF" required="true">
            <?php } ?>
          <button class="w-100 btn-primary btn-lg mt-2" type="submit" id="btnLogin">Login</button>
        </form>
        <p>Ainda não tem cadastro? Cadastre-se <a href="/src/views/<?php echo $type; ?>">aqui</a></p>
      </div>

    <?php } else { ?>
      <div class="card-body w-50">
        <h3 class='sub-title'>Olá <b><?php echo $userName ?></b></h3>
        <p>Aguarde, estamos te redirecionando para a tela principal...</p>
      </div>
    <?php } ?>

  </div>
</div>

<script type="application/javascript">
  "use-strict"
  <?php
  // variavel definida pelo template header.php
  // se o usuário ja estiver logado forca o redirecionamento para a tela principal
  if ($isLoggedIn || !isset($type)) {
  ?>
    setTimeout(() => {
      window.location.href = "/src/"
    }, 0)
  <?php } ?>
  
  const login = async (e) => {
    try {
      const isFormAluno = "<?php echo $isFormAluno?>" == "1";
      e.preventDefault();
      const email = document.getElementById("email")?.value;
      const senha = document.getElementById("password")?.value;
      const cpf = document.getElementById("cpf")?.value || "";

      let url = "/api/routers/login/";

      // de for formulario de aluno troca url de post
      if (isFormAluno) {
        url = "/api/routers/login-aluno/";
      }

      const {
        data
      } = await axios.post(url, {
        email,
        senha,
        cpf: cpf.replace(/[.]|[-]/g, "")
      });

      swal.fire({
        html: `Olá <b>${data?.result?.professor?.nome ?? data?.result?.User?.nome}</b>, estamos te redirecionando para a pagina principal!`,
        icon: "success",
        showConfirmButton: false,
        allowEnterKey: false,
        allowEscapeKey: false,
        allowOutsideClick: false,
      });

      setTimeout(() => {
        window.location.href = "/src/"
      }, 3000);
    } catch (e) {
      console.warn(e);
      swal.fire({
        text: `Falha ao fazer login. Causa: ${e?.response?.data?.error ?? e.message}`,
        icon: "warning"
      });
    }
  }

  (() => {
    document.getElementById("formSubmit").addEventListener("submit", login);
  })()
</script>
<?php require_once("./templates/footer.php") ?>