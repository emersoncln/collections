(() => {
  document.querySelector("form").addEventListener("submit", async (e) => {
    e.preventDefault();
    const nome = document.getElementById("firstName")?.value;
    const sobrenome = document.getElementById("lastName")?.value;
    const cpf = document.getElementById("cpf")?.value.replace(/[.]|[-]/g, "");
    const turma = document.getElementById("turma")?.value;

    swal.fire({
      text: "salvando aluno...",
      allowScapeKey: false,
      allowEnterKey: false,
      allowOutsideClick: false,
    });
    swal.showLoading();
    try {
      const { data } = await axios.post("/api/routers/cadastrar-aluno/index.php", {
        nome,
        sobrenome,
        cpf,
        turma,
      });
      console.log(data);
      swal.close();
      swal.fire({
        text: "Salvo com sucesso, redirecionando para a página principal",
        icon: "success",
        showConfirmButton: false,
        allowEnterKey: false,
        allowEscapeKey: false,
        allowOutsideClick: false,
      });
      setTimeout(() => {
        window.location.href = "/src/";
      }, 2000);
    } catch (e) {
      swal.fire({
        text: `Falha ao salvar aluno. Causa: ${
          e?.response?.error ?? e?.message
        }`,
        icon: "warning",
      });
    }
  });
})();
