<?php
include "../../../vendor/autoload.php";

use src\Model\Turma;
$title="Cadastrar novo aluno";
include_once("../../templates/header.php");
?>
<div class='container'>
  <div class="card block" style="width: 100%;">
    <h1 class='title'>Cadastre-se</h1>
    <h3 class='sub-title'>preencha os campos abaixo</h3>
    <div class="card-body w-50">
      <form>
        <label for="firstName" class="form-label">Nome</label>
        <input type="text" class="form-control" id="firstName" placeholder="" value="" required="">
        <label for="lastName" class="form-label">Sobrenome</label>
        <input type="lastName" class="form-control" id="lastName" placeholder="" value="" required="">
        <label for="number" class="form-label">CPF</label>
        <input type="text" class="form-control" id="cpf" pattern="[0-9]{3}\.[0-9]{3}\.[0-9]{3}-[0-9]{2}" placeholder="" value="" required="">
        <label for="turma" class="form-label">Turma</label>
        <select aria-readonly="false" class="form-select" name="turma" id="turma" required="true" value="">
          <option value="">Selecione uma opção</option>
          <?php

          $turmaModel = new Turma();
          $turmas = $turmaModel->getTurmas();

          foreach ($turmas as $turma) {
          ?>
            <option value="<?php echo $turma["id"]; ?>"><?php echo $turma["nome"]; ?></option>
          <?php } ?>
        </select>

        <button class="w-100 btn-primary btn-lg mt-2" type="submit">Cadastrar</button>
      </form>
      <span>Já tem cadastro?</span>
      <a href="/src/login.php?type=alunos">Login</a>
    </div>
  </div>
</div>
<script src="alunos.js"></script>
</body>

</html>