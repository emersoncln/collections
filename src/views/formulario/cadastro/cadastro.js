"use strict"
const listaItens = [];
const turma = document.getElementById("turma");
const titulo = document.getElementById("titulo");
const descricao = document.getElementById("descricao");
const disciplina = document.getElementById("disciplina");
const questao = document.getElementById("questao");
const correta = document.getElementById("correta");
const tblItens = document.querySelector(".tbl-itens");
const alertNoItens = document.getElementById("no-itens");
const btnClose = document.querySelector(".btn-close");
const btnSalvarQuestionario = document.querySelector(".btn-savar-form");
const btnCancelarQuestionario = document.querySelector(".btn-cancelar-form");
const form = document.querySelector("form");
const itensJson = document.getElementById("itensJson")

/**
 * @type {HTMLButtonElement}
 */
const btnAddItem = document.querySelector(".btn-add-item");

const limparCampos = () => {
    questao.value = "";
    correta.checked = false;
}

const getRowItem = (item, index) => {
    return `<tr>
        <th scope="row">${index}</th>
        <td>${item.questao}</td>
        <td>${item.correta ? "sim" : "Não"}</td>
        <td>
            <!-- descomentar a linha abaixo para adicionar o botao de editar
             precisa adicionar as ações
             <button class="btn btn-primary" data-index="${index}">Editar</button> -->
            <button class="btn btn-danger btn-remove-itens" data-index="${index}">Remover</button>
        </td>
    </tr>`
}

const renderList = () => {
    let html = listaItens.map((row, i) => getRowItem(row, i + 1)).join(" ");
    tblItens.querySelector("tbody").innerHTML = html;

    if (listaItens.length > 0) {
        alertNoItens.classList.add("d-none");
    } else {
        alertNoItens.classList.remove("d-none");
    }

    let hasCorreta = listaItens.find(item => item.correta);
    if (hasCorreta) {
        correta.setAttribute("disabled", "true")
    } else {
        correta.removeAttribute("disabled")
    }

    let json = JSON.stringify(listaItens)
    itensJson.value = json;
}

const removeItem = async (index) => {
    const resp = await swal.fire({
        html: `Tem certeza que deseja remover o item <b>${listaItens[index].questao}</b>?`,
        icon: "question",
        showCancelButton: true,
        cancelButtonText: "Cancelar",
        confirmButtonText: "Remover",
    });

    if (resp.value) {
        listaItens.splice(index, 1);
        renderList();
    }
}

const addItem = (e) => {
    e.preventDefault();

    let item = {
        questao: questao.value,
        correta: correta.checked,
    };

    listaItens.push(item);
    renderList();
    btnClose.click();
    limparCampos();
    
}
const onCancel = async (e) => {
    e.preventDefault();
    const resp = await swal.fire({
        html: `Tem certeza que deseja cancelar?`,
        icon: "question",
        showCancelButton: true,
        cancelButtonText: "Ficar no formulário",
        confirmButtonText: "Cancelar e voltar",
    });

    if (resp.value) {
        window.history.go(-1);
    }
}
document.body.addEventListener("click", (e) => {
    if (e.target.classList.contains("btn-remove-itens")) {
        removeItem(e.target.dataset.index - 1);
    }
});

btnAddItem.addEventListener("click", addItem);
btnCancelarQuestionario.addEventListener("click", onCancel)
form.addEventListener("submit", async (e) => {
    e.preventDefault();
    swal.fire({
        text: "Salvando questionário...",
        allowScapeKey: false,
        allowEnterKey: false,
        allowOutsideClick: false,
      });
      swal.showLoading();
    try {
    await axios.post("/api/routers/cadastrar-questionario/index.php", {
        turma: turma.value,
        titulo: titulo.value,
        descricao: descricao.value,
        questoes: listaItens,
        disciplina: disciplina.value,
    });
    swal.close();
      swal.fire({
        text: "Salvo com sucesso, redirecionando...",
        icon: "success",
        showConfirmButton: false,
        allowEnterKey: false,
        allowEscapeKey: false,
        allowOutsideClick: false,
      });
      setTimeout(() => {
        history.go(-1);
      }, 2000);
    } catch (e) {
    swal.fire({
        text: `Falha ao salvar questionario. Causa: ${
            e?.response?.error ?? e?.message
        }`,
        icon: "warning",
        });
    }
});