<?php
include "../../../../vendor/autoload.php";

use src\Model\Disciplina;
use src\Model\Turma;

$title = "Cadastro de questionário";
include_once("../../../templates/header.php");

$turmaSelecionada = $_GET["turma_id"]
?>
<div class='container'>
  <form method="POST" action="/api/routers/cadastrar-questionario/index.php">
    <div class="card block" style="width: 100%;">
      <h1 class='title'>Cadastrar questionário</h1>
      <div class="card-body w-100" style="overflow: auto;">

        <label for="turma" class="form-label">Turma</label>

        <select disabled aria-readonly="false" class="form-select mb-3" name="turma" id="turma" required="true">
          <option value="">Selecione uma turma</option>
          <?php
          $turmaModel = new Turma();
          $turmas = $turmaModel->getTurmaByProfId($professorId);

          foreach ($turmas as $turma) {
          ?>
            <option <?php echo (($turmaSelecionada == $turma["id"]) ? "selected" : ""); ?> value="<?php echo $turma["id"]; ?>"><?php echo $turma["nome"]; ?></option>
          <?php } ?>
        </select>

        <div class="mb-3">
          <label for="titulo" class="form-label text-dark">Titulo</label>
          <input type="text" class="form-control" name="titulo" id="titulo" placeholder="">
        </div>

        <div class="mb-3">
          <label for="descricao" class="form-label text-dark">Descrição</label>
          <textarea type="text" class="form-control" name="descricao" id="descricao" rows="3"></textarea>
        </div>

        <select aria-readonly="false" class="form-select mb-3" name="disciplina" id="disciplina" required="true">
          <option value="">Selecione uma disciplina</option>
          <?php
          $disciplinaModel = new Disciplina();
          $disciplinas = $disciplinaModel->getDisciplinas();

          foreach ($disciplinas as $disciplina) {
          ?>
            <option value="<?php echo $disciplina["id"]; ?>"><?php echo $disciplina["nome"]; ?></option>
          <?php } ?>
        </select>

        <div class="mb-3">
          <h4 class="form-label text-dark">Opções de respostas</h4>
          <!-- <input type="text" class="form-control hide" id="resposta" placeholder=""> -->
          <button type="button" data-bs-toggle="modal" data-bs-target="#staticBackdrop" class="btn btn-primary mt-2">Adicionar resposta</button>
        </div>

        <!-- Vertically centered modal -->
        <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Adicionar uma resposta</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
              </div>
              <div class="modal-body">
                <div class="row">

                  <div class="col-12 mb-3">
                    <label for="questao" class="form-label text-dark">Questão</label>
                    <textarea class="form-control" id="questao" rows="3"></textarea>
                  </div>

                  <div class="col-12 mb-3">
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="" id="correta" />
                      <label class="form-check-label" for="correta">
                        É a afirmativa correta?
                      </label>
                    </div>

                    <div class="alert alert-warning" role="alert">
                      Apenas 1 correta é permitida
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary btn-add-item">Adicionar</button>
              </div>
            </div>
          </div>
        </div>

        <table class="table table-striped tbl-itens">
          <thead>
            <tr>
              <th scope="col" class="col-1">#</th>
              <th scope="col" class="col-8">Questão</th>
              <th scope="col" class="col-1">Correta</th>
              <th scope="col" class="col-2">Ações</th>
            </tr>
          </thead>
          <tbody style="max-width: 500px; overflow: auto;">

          </tbody>
        </table>
        <div id="no-itens" class="alert alert-dark" role="alert">
          Nenhum item adicionado
        </div>
      </div>
      <input class="fake-input-to-save-itens dont-remove d-none" name="questoes" id="itensJson" />
      <div class="card-footer w-100">
        <button class="btn-danger btn-lg mt-2 btn-cancelar-form" type="">Cancelar</button>
        <button class="btn-primary btn-lg mt-2 btn-savar-form" type="submit">Salvar</button>
      </div>
    </div>
  </form>
</div>

<script src="./cadastro.js" type="application/javascript"></script>
<?php include_once("../../../templates/footer.php") ?>