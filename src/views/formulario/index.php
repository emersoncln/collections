<?php
include "../../../vendor/autoload.php";

use src\Model\Questionario;
use src\Model\Turma;

$title = "Cadastro de questionário";
include_once("../../templates/header.php");

if (isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    $id = null;
}

$turmaModel = new Turma();
$turma = $turmaModel->getTurmaById($id);

?>

<body>
    <main class="container mt-5">
        <div class="card mb-3">
            <div class="card-header">
                <div class="d-flex justify-content-between">
                    <h3 class='sub-title'>Formulários</h3>
                    <button onclick="onClickAccessCadastroFormulario()" type="button" class="btn btn-primary position-relative">Adicionar</button>
                </div>
                <h6 class='sub-title'><?php echo $turma["id"] . ' - ' . $turma["nome"]; ?></h6>
            </div>
            <div class="card-body">
                <div class="question-container row">
                
                    <?php

                    $QuestionarioModel = new Questionario();
                    $Questionarios = $QuestionarioModel->getQuestionariosTurmaId($id);

                    foreach ($Questionarios as $Questionario) {
                    ?>
                        <div class="col col-12 col-md-6 col-lg-4" style="margin-bottom: 10px;">
                            <div class="card d-flex shadow text-muted m-2">
                                <h5 class="card-header"><?php echo $Questionario["titulo"]; ?></h5>
                                <!-- <strong class="d-block"></strong> -->
                                <div class="card-body">
                                    <div class="card-text"><b>Descrição: </b><?php echo $Questionario["descricao"] ?></div>
                                    <div class="card-text"><b>Disciplina: </b><?php echo $Questionario["disciplina"] ?></div>
                                </div>
                            </div>
                        </div>
                    <?php }

                    if (count($Questionarios) == 0) {
                    ?>
                        <div class="alert alert-primary">
                            <h4 class="alert-heading">Nenhum questionário para esta turma</h4>
                            <p>Clique no botão superior "<b>Adicionar</b>" para cadastrar um questionário</p>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="card-footer">
                <a href="javascript:void(0)" onclick="history.go(-1); return false;" type="button" class="btn btn-secondary">voltar</a>
            </div>
        </div>
    </main>
    <script src="formulario.js"></script>
    <script>
        function onClickAccessCadastroFormulario() {
            window.location.href = "/src/views/formulario/cadastro/?turma_id=<?php echo $id ?>"
        }
    </script>
</body>

</html>