// (() => {
//   document.querySelector("form").addEventListener("submit", async (e) => {
//     e.preventDefault();
//     const titulo = document.getElementById("titulo")?.value;
//     const descricao = document.getElementById("descricao")?.value;
//     const questoes = document.getElementById("resposta")?.value;
//     const turma = document.getElementById("turma")?.value;

//     swal.fire({
//       text: "Salvando questionário...",
//       allowScapeKey: false,
//       allowEnterKey: false,
//       allowOutsideClick: false,
//     });
//     swal.showLoading();
//     try {
//       const { data } = await axios.post("/api/routers/cadastrar-questionario/index.php", {
//         titulo,
//         descricao,
//         questoes,
//         turma,
//       });
//       console.log(data);
//       swal.close();
//       swal.fire({
//         text: "Salvo com sucesso, redirecionando para a página principal",
//         icon: "success",
//         showConfirmButton: false,
//         allowEnterKey: false,
//         allowEscapeKey: false,
//         allowOutsideClick: false,
//       });
//       setTimeout(() => {
//         window.location.href = "/src/";
//       }, 2000);
//     } catch (e) {
//       swal.fire({
//         text: `Falha ao salvar aluno. Causa: ${
//           e?.response?.error ?? e?.message
//         }`,
//         icon: "warning",
//       });
//     }
//   });
// })();
