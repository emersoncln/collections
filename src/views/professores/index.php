<?php 
$title="Professores";
include_once("../../templates/header.php")
?>

<div class='container'>
  <div class="card block" style="width: 100%;">
    <h1 class='title'>Cadastre-se</h1>
    <h3 class='sub-title'>preencha os campos abaixo</h3>
    <div class="card-body w-50">
      <label for="firstName" class="form-label">Nome</label>
      <input type="text" class="form-control" id="firstName" placeholder="" value="" required="">
      <label for="lastName" class="form-label">Sobrenome</label>
      <input type="lastName" class="form-control" id="lastName" placeholder="" value="" required="">
      <label for="number" class="form-label">Telefone</label>
      <input type="tel" class="form-control phone" id="phone" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}" placeholder="" value="" required="">
      <label for="email" class="form-label">E-mail</label>
      <input type="email" class="form-control" id="email" placeholder="">
      <label for="password" class="form-label">Senha</label>
      <input type="password" class="form-control" id="password" placeholder="">
      <button class="w-100 btn-primary btn-lg mt-2" type="submit">Cadastrar</button>
      <span>Já tem cadastro?</span>
      <a href="/src/login.php?type=professores" onclick="">Login</a>
    </div>
  </div>
</div>

<script src="./professores.js" type="application/javascript"></script>
<?php include_once("../../templates/footer.php") ?>