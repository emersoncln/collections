<?php
include "../../../vendor/autoload.php";
include_once("../../templates/header.php");

use src\Model\Questionario;

$quest = "";

if (isset($_GET["q"])) {
    $questId = $_GET["q"];
    $quest = (new Questionario())->getQuestionarioById($questId);
}

if (isset($quest)) {
    $items = (new Questionario())->getItemsQuestionarios($questId, $alunoId);
}

$questNome = $quest["titulo"];
$title = "Responder questionário $questNome";

?>

<div class="container">
    <div class="card block" style="width: 100%; justify-content: space-between; padding: 0;">
        <div class="card-header" style="width: 100%;">
            <h5><?php echo $quest["titulo"] ?></h5>
        </div>

        <?php
        if (!isset($items) || count($items) == 0) {
        ?>
            <div class="card-body" style="width: 100%;">
                <div class="alert alert-warning " style="width: 90%;" role="alert">
                    <h5>Nenhuma alternativa para esta questão</h5>
                    <p>Verifique com o seu professor!</p>
                </div>
            </div>
            <div class="card-footer" style="width: 100%;">
                <a href="/src/" class="btn btn-danger">Voltar</a>
            </div>
        <?php
        } else {
        ?>
            <div class="card-body" style="width: 100%;">
                <p class="card-text"><?php echo $quest["descricao"] ?></p>
                <h6>Marque a alternativa correta:</h6>
                <ul class="list-group " style="width: 100%;">
                    <?php
                    foreach ($items as $item) { ?>
                        <li class="list-group-item">
                            <?php if (!empty($item["resposta_id"])) { ?>
                                <input value="<?php echo $item["resposta_id"] ?>" class="d-none" id="id" name="id">
                            <?php } ?>
                            <input data-ok="<?php echo $item["correta"] ?>" type="radio" id="<?php echo $item["id"] ?>" name="resposta">
                            <label for="<?php echo $item["id"] ?>"><?php echo $item["questao"] ?></label>
                        </li>
                    <?php }
                    ?>
                </ul>
            </div>

            <div class="card-footer" style="width: 100%;">
                <a href="/src/" class="btn btn-danger">Desistir</a>
                <a href="#" class="btn btn-responder btn-success">Responder</a>
            </div>
        <?php } ?>
    </div>
</div>

<script src="./questionario.js" type="application/javascript"></script>

<?php include_once("../../templates/footer.php") ?>