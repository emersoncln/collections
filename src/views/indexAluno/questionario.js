"use strict"
const botaoResponder = document.querySelector(".btn-responder");
const onClickBotaoResponder = async (e) => {
  e.preventDefault();

  swal.fire({
    text: "Salvando questionário...",
    allowScapeKey: false,
    allowEnterKey: false,
    allowOutsideClick: false,
  });
  swal.showLoading();
  try {
    const questItemId = document.querySelector("input[name=resposta]:checked");
    const id = document.querySelector("#id");

    if (!questItemId) {
        swal.fire({
            text: "Por favor selecione uma alternativa!",
            allowOutsideClick: false,
            icon: "warning"
        });

        return;
    }

    await axios.post("/api/routers/salvar-resposta/index.php", {
      questItemId: questItemId.id,
      id: id?.value
    });
    swal.close();
    swal.fire({
      text: "Salvo com sucesso, redirecionando...",
      icon: "success",
      showConfirmButton: false,
      allowEnterKey: false,
      allowEscapeKey: false,
      allowOutsideClick: false,
    });
    setTimeout(() => {
      location.href = "/src/";
    }, 2000);
  } catch (e) {
    swal.fire({
      text: `Falha ao salvar questionario. Causa: ${
        e?.response?.error ?? e?.message
      }`,
      icon: "warning",
    });
  }
};
console.log(botaoResponder)
botaoResponder.addEventListener("click", onClickBotaoResponder);
