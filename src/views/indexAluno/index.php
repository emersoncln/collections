<?php

use src\Model\Questionario;
use src\Model\Turma;

$turma_id = $_SESSION["turma_Id"];


$questionarioNegocio = new Questionario();
$TurmaNegocio = new Turma();

$turmaData = $TurmaNegocio->getTurmaById($turma_id);
$questionarios = $questionarioNegocio->getQuestionariosTurmaId($turma_id, $alunoId)

?>
<div><b>Turma</b>: <?php echo $turmaData["nome"] ?></div>

<div class="row" style="width: 100%;">
    <?php
    foreach ($questionarios as $quest) { ?>
        <div class="col col-12 col-md-6 col-lg-4" style="margin-bottom: 10px;">
            <div class="card">
                <h5 class="card-header"><?php echo $quest["titulo"] ?></h5>
                <div class="card-body">
                    <div class="card-text"><b>Descrição: </b><?php echo $quest["descricao"] ?></div>
                    <div class="card-text"><b>Disciplina: </b><?php echo $quest["disciplina"] ?></div>
                    <?php
                    if ($quest["respondido"] != null) {
                    ?>
                        <hr>
                        <div class="card-text"><b>Minha resposta: </b><?php echo $quest["respondido"] ?></div>
                        <div class="card-text"><b>Correta: </b><?php echo $quest["respondido_correta"] ?></div>
                    <?php } ?>
                </div>
                <div class="card-footer">
                    <form action="./views/indexAluno/questionario.php" method="get">
                        <input type="text" style="display: none;" name="q" value="<?php echo $quest["quest_id"] ?>">
                        <?php
                        if ($quest["respondido"] != null) {
                        ?>
                            <button class="btn btn-success" type="submit">Responder novamente</button>
                        <?php } else { ?>
                            <button class="btn btn-success" type="submit">Responder</button>
                        <?php } ?>
                    </form>
                </div>
            </div>
        </div>
    <?php

    }

    ?>
</div>