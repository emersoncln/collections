<?php
$title = "Cadastro de disciplinas";
include_once("../../../src/templates/header.php");
?>

<body>
    <main class="container mt-5">
    <form method="POST" action="/api/routers/cadastrar-turmas/index.php" >
        <div class="card mb-3">
            <div class="card-header">
            <h3 class='sub-title'>Cadastrar disciplina</h3>
            </div>
            <div class="card-body">
                <div class="mb-3">
                    <label for="titulo" class="form-label text-dark">Disciplina</label>
                    <input type="text"  class="form-control" id="disciplina" placeholder="">
                </div>
            </div>
            <div class="card-footer">
                <a  type="button" class="btn btn-secondary btn-voltar">voltar</a>
                <button type="submit" class="btn btn-primary">Cadastrar</button>
            </div>
        </div>
    </form>
    </main>
    <script src="./disciplina.js"></script>
    <?php include_once("../../templates/footer.php") ?>
</body>

</html>
