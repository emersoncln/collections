"use strict"
const turma = document.getElementById("disciplina");
const btnoltar = document.querySelector(".btn-voltar");
const form = document.querySelector("form");

/**
 * @type {HTMLButtonElement}
 */
const limparCampos = () => {
    disciplina.value = "";
}

const onVoltar = async (e) => {
    e.preventDefault();
    const resp = await swal.fire({
        html: `Tem certeza que deseja voltar?`,
        icon: "question",
        showCancelButton: true,
        cancelButtonText: "Ficar no cadastro de disciplinas",
        confirmButtonText: "Cancelar e voltar",
    });

    if (resp.value) {
        window.history.go(-1);
    }
}

btnoltar.addEventListener("click", onVoltar);
form.addEventListener("submit", async (e) => {
    e.preventDefault();
    swal.fire({
        text: "Salvando questionário...",
        allowScapeKey: false,
        allowEnterKey: false,
        allowOutsideClick: false,
      });
      swal.showLoading();
    try {
    await axios.post("/api/routers/cadastrar-disciplina/index.php", {
        nome: disciplina.value,
    });
    limparCampos();
    swal.close();
      swal.fire({
        text: "Salvo com sucesso, redirecionando...",
        icon: "success",
        showConfirmButton: false,
        allowEnterKey: false,
        allowEscapeKey: false,
        allowOutsideClick: false,
      });
      setTimeout(() => {
        history.go(-1);
      }, 2000);
    } catch (e) {
    swal.fire({
        text: `Falha ao salvar questionario. Causa: ${
            e?.response?.error ?? e?.message
        }`,
        icon: "warning",
        });
    }
});