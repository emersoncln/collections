<?php
include "../../../vendor/autoload.php";
use src\Model\Professor;

$title = "Cadastro de turmas";
include_once("../../../src/templates/header.php");
?>

<body>
    <main class="container mt-5">
        <form method="POST" action="/api/routers/cadastrar-turmas/index.php">
            <div class="card mb-3">
                <div class="card-header">
                    <h3 class='sub-title'>Cadastrar turma</h3>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <label for="titulo" class="form-label text-dark">Turma</label>
                        <input type="text" class="form-control" id="turma" placeholder="">
                        <label for="titulo" class="form-label text-dark">Professor</label>
                        <select disabled aria-readonly="false" class="form-select mb-3" name="professor" id="professor" required="true">
                            <option value="">Selecione um professor</option>
                            <?php
                            $professorModel = new Professor();
                            $professores = $professorModel->getProfessores();

                            foreach ($professores as $professor) {
                            ?>
                                <option <?php echo (($professorId == $professor['id']) ? 'selected' : '') ?> value="<?php echo $professor['id']; ?>"><?php echo $professor['nome'] . ' ' . $professor['sobrenome']; ?></option>
                            <?php } ?>
                        </select>

                    </div>
                </div>
                <div class="card-footer">
                    <a type="button" class="btn btn-secondary btn-voltar">voltar</a>
                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                </div>
            </div>
        </form>
    </main>
    <script src="./turmas.js"></script>
    <?php include_once("../../templates/footer.php") ?>
</body>

</html>