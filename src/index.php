<?php
// carrega o template do header
include "../vendor/autoload.php";

use src\Model\Turma;

$title = "Acesso";
include "../vendor/autoload.php";
include_once("./templates/header.php");
?>
<body>
<link href="aluno-card.css" rel="stylesheet">
<div class='container'>
  <div class="card block p-0" style="width: 100%;">
    <?php if (!$isLoggedIn) { ?>
      <h1 class='title'>Collections</h1>
    <?php } ?>
    <?php if ($isLoggedIn) { ?>
      <div class="card-header mb-2 w-100 d-flex justify-content-between">
        <h5 class='sub-title'>Bem vindo <?php echo "<b>" . $userName . "</b>" ?>, esta é sua plataforma de questionários online</h5>
        <div class="d-flex justify-content-between" >
        <?php if ($isProfessor) { ?>
          <a onclick="onClickAccessCadastroTurmas()" type="button" class="btn btn-primary position-relative ">Cadastrar turma</a>
          <a onclick="onClickAccessCadastroDisciplinas()" type="button" class="btn btn-primary position-relative" style="margin-left:10px;">Cadastrar Disciplina</a>
        <?php
        }
        ?>
        <a href="/api/routers/logout/" type="button" style="margin-left:10px;" class="btn btn-primary position-relative ">Sair</a>
        </div>
      </div>
      <?php if (!$isProfessor) { ?>
        <h3 class='sub-title'>Meus questionários</h3>
      <?php
        include_once("../src/views/indexAluno/index.php");
      } else { ?>
        <h3 class='sub-title'>Minhas Turmas</h3>
      <?php
      }
      ?>
    <?php } else { ?>
      <h3 class='sub-title'>Bem vindo, a sua plataforma de questionários online</h3>
    <?php } ?>

    <?php if (!$isLoggedIn) { ?>
      <img src="assets/checklist.svg" class="" alt="imagem">
    <?php } ?>
    <div class="card-body" <?php if ($isLoggedIn) { ?> style="width: 100%;" <?php } ?>>

      <?php if (!$isLoggedIn) { ?>
        <h4 class='sub-title text-center'>Acessar como</h4>
        <div class='footer'>
          <button id="btnAluno" type="button" class="btn btn-secondary" onclick="onClickAluno">Aluno</button>
          <button id="btnProfessor" type="button" class="btn btn-primary" onclick="onClickProfessor">Professor</button>
        </div>
      <?php } else if ($isProfessor) { ?>
        <!-- <h4 class='sub-title text-center'>Minhas turma</h4> -->
        <div class="container">
          <div class="row">
            <?php

            $turmaModel = new Turma();
            $turmas = $turmaModel->getTurmas();

            foreach ($turmas as $turma) {
            ?>
              <div class="col col-md-6 col-lg-4">
                <div onclick="onClickAccessFormulario('<?php echo $turma['id']; ?>')" id="<?php echo $turma["id"]; ?>" class="card d-flex item-aluno">
                  <div class="card-body" style="width: 100%;">
                    <label><?php echo $turma["nome"]; ?></label>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </div>
      <?php } ?>
    </div>
  </div>
</div>

<script type="application/javascript">
  /**
   * @param {string} type
   * @return {(e: Event) => void}
   */

  function onClickAccessFormulario (id) {
    window.location.href = `/src/views/formulario?id=${id}`
  }

  function onClickAccessCadastroDisciplinas () {
    window.location.href = `/src/views/disciplina`;
  }

  function onClickAccessCadastroTurmas () {
    window.location.href = `/src/views/turmas`;
  }
  const onClickAccessType = (type) => (e) => {
    e.preventDefault();
    window.location.href = `/src/login.php?type=${type}`
  }
  document.getElementById("btnAluno").addEventListener("click", onClickAccessType("alunos"))
  document.getElementById("btnProfessor").addEventListener("click", onClickAccessType("professores"))
</script>

</body>

</html>
